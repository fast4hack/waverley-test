# Getting Started

### Technologies
In this project we have used such common technologies like:
 * [Java 8] 
 * [Spring Boot] - framework used to create a microservice
 * [Spring Cloud OpenFeign] - simplify interacting with third-party API
 * [Spring Cache] - caching using Spring
 * [Maven] - tool for building Java-based projects
 * [Junit] - unit testing framework
 * [Mockito] - mocking framework
 * [Lombok] - is used to reduce boilerplate code

### Scaling
Now application have infrastructure that enough for not large volume of requests.
But, if we want to scale it, we should replace spring caching with some external storage(e.g. Redis). It allows us to run few instances of app and manage requests with some loadbalancer. 
Also in future versions we should config Dockerfile to run it in Docker without dependency to local environment.

### Data-flow

Data flow [diagram](https://drive.google.com/file/d/1K9rSJ-Rl2ujbcn9Kyj95ZR9eM1xiPmHJ/view?usp=sharing)

### Installation
To install application you need to have OpenJDK 8 and Maven installed on your machine.
To install it, use following commands: 
```sh
$ sudo apt install openjdk-8-jdk
$ sudo apt install maven
```
### Build
To build application run following command from project root:
```sh
$ mvn clean install -DskipTests=true
```
### Run
To run built application use:
```sh
$ java -jar ./target/soft-0.0.1-SNAPSHOT.jar
```

### Tests
You can run unit and integrations tests with command:
```sh 
$ mvn test
```

### API
#### Request
GET /welcome/{ip} HTTP/1.1

##### Example:
```
curl -v http://localhost:8080/welcome/63.70.164.200
curl -v http://localhost:8080/welcome/77.120.21.172
```

#### Response Codes
```
200: Success
400: Bad request
```

#### Example Error Messages
```
HTTP/1.1 400 BAD_REQUEST
{
    "errorCode": 400,
    "description": "Weather was not determined for your location!"
}
```
```
HTTP/1.1 400 BAD_REQUEST
{
    "errorCode": 400,
    "description": "Location was not determined by IP!"
}
```

```
HTTP/1.1 400 BAD_REQUEST
{
    "errorCode": 400,
    "description": "IP address is not valid!"
}
```

#### Successful Response
```
HTTP/1.1 200 OK
{
    "message": "Welcome, it looks Clouds in Khmelnytskyi, Ukraine today"
}
```

 [Java 8]: <https://docs.oracle.com/javase/8/docs/>
 [Spring Boot]: <https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/>
 [Spring Cloud OpenFeign]: <https://cloud.spring.io/spring-cloud-openfeign/reference/html/>
 [Spring Cache]: <https://spring.io/guides/gs/caching/>
 [Maven]: <https://maven.apache.org/guides/index.html>
 [Junit]: <https://junit.org/junit5/docs/current/user-guide/>
 [Mockito]: <https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html>
 [Lombok]: <https://www.projectlombok.org/features/all>
