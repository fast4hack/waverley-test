package com.waverley.soft.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class WelcomeMessageDTO {
    private String message;
}
