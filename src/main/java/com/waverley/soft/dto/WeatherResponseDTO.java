package com.waverley.soft.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WeatherResponseDTO {

    private List<Weather> weather;

}

