package com.waverley.soft.controller;

import com.waverley.soft.dto.WelcomeMessageDTO;
import com.waverley.soft.service.WelcomeMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@Slf4j
@RestController
@RequiredArgsConstructor
public class WelcomeMessageController {

    private final WelcomeMessageService welcomeMessageService;

    @GetMapping("/welcome/{ip}")
    public Callable<ResponseEntity<WelcomeMessageDTO>> getWelcomeMessage(@PathVariable("ip") String ip) {
        log.info("Income request for welcome message for user with ip {}", ip);

        return () -> {
            WelcomeMessageDTO message = welcomeMessageService.getWelcomeMessageByIP(ip);
            return new ResponseEntity<>(message, HttpStatus.OK);
        };
    }
}
