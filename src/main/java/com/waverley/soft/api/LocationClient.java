package com.waverley.soft.api;

import com.waverley.soft.dto.LocationResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Client that send HTTP requests to Location API
 */
@FeignClient(name = "locationClient", url = "${ip.lookup.url}")
public interface LocationClient {
    /**
     * Send request to Location API to determine location by IP address
     *
     * @param ip IP address
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{ip}", consumes = "application/json")
    LocationResponseDTO getLocation(@PathVariable("ip") String ip);
}
