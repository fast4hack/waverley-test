package com.waverley.soft.api;

import com.waverley.soft.dto.WeatherResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Client that send HTTP requests to Weather API
 */
@FeignClient(name = "weatherClient", url = "${weather.api.url}")
public interface WeatherClient {
    /**
     * Send request to Weather API to get current weather description
     * @param location city name
     * @param apiKey API key for Weather API
     */
    @RequestMapping(method = RequestMethod.GET, value = "/", consumes = "application/json")
    WeatherResponseDTO getWeather(@RequestParam(value = "q") String location, @RequestParam(value = "APPID") String apiKey);
}
