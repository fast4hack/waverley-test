package com.waverley.soft.service;

import com.waverley.soft.dto.LocationResponseDTO;

/**
 * Service for determining user location
 */
public interface LocationService {
    /**
     * Determine location by IP address
     * @param ip IP address
     * @return location response
     */
    LocationResponseDTO getLocationByIP(String ip);
}
