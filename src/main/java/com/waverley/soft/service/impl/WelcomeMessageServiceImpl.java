package com.waverley.soft.service.impl;

import com.waverley.soft.dto.LocationResponseDTO;
import com.waverley.soft.dto.WelcomeMessageDTO;
import com.waverley.soft.service.LocationService;
import com.waverley.soft.service.WeatherService;
import com.waverley.soft.service.WelcomeMessageService;
import com.waverley.soft.validation.IPAddressValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WelcomeMessageServiceImpl implements WelcomeMessageService {
    @Value("${welcome.message.template}")
    private String WELCOME_MESSAGE_TEMPLATE;

    private final WeatherService weatherService;
    private final LocationService locationService;
    private final IPAddressValidator ipAddressValidator;

    @Override
    public WelcomeMessageDTO getWelcomeMessageByIP(String ip) {
        ipAddressValidator.validate(ip);
        log.info("Generating welcome message for ip {}", ip);
        LocationResponseDTO location = locationService.getLocationByIP(ip);
        String weatherDescription = weatherService.getWeatherDescriptionByCity(location.getCity());
        return generateWelcomeMessage(location, weatherDescription);
    }

    private WelcomeMessageDTO generateWelcomeMessage(LocationResponseDTO location, String weatherDescription) {
        return WelcomeMessageDTO
            .builder()
            .message(String.format(WELCOME_MESSAGE_TEMPLATE, weatherDescription, location.getCity(), location.getCountry()))
            .build();
    }
}
