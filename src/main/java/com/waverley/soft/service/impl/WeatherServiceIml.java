package com.waverley.soft.service.impl;

import com.waverley.soft.api.WeatherClient;
import com.waverley.soft.dto.WeatherResponseDTO;
import com.waverley.soft.service.WeatherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherServiceIml implements WeatherService {
    @Value("${weather.api.key}")
    private String WEATHER_API_KEY;

    private final WeatherClient weatherClient;

    @Cacheable("weathers")
    @Override
    public String getWeatherDescriptionByCity(String city) {
        log.info("Sending request to Weather API for location - {}", city);
        WeatherResponseDTO weatherResponseDTO = weatherClient.getWeather(city, WEATHER_API_KEY);
        return getWeatherDescriptionFromResponse(weatherResponseDTO);
    }

    private String getWeatherDescriptionFromResponse(WeatherResponseDTO weatherResponseDTO) {
        return weatherResponseDTO.getWeather().get(0).getDescr();
    }
}
