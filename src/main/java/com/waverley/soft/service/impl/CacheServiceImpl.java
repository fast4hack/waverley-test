package com.waverley.soft.service.impl;

import com.waverley.soft.service.CacheService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CacheServiceImpl implements CacheService {

    private final CacheManager cacheManager;

    @Override
    public void evictLocationCaches() {
        cacheManager.getCache("locations").clear();
    }

    @Override
    public void evictWeatherCaches() {
        cacheManager.getCache("weathers").clear();
    }

    @Scheduled(fixedRate = 600000)
    public void evictLocationCachesAtIntervals() {
        evictLocationCaches();
    }

    @Scheduled(fixedRate = 600000)
    public void evictWeatherCachesAtIntervals() {
        evictWeatherCaches();
    }
}
