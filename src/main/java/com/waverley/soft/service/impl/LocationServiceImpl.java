package com.waverley.soft.service.impl;

import com.waverley.soft.api.LocationClient;
import com.waverley.soft.dto.LocationResponseDTO;
import com.waverley.soft.service.LocationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {
    private final LocationClient locationClient;

    @Cacheable("locations")
    @Override
    public LocationResponseDTO getLocationByIP(String ip) {
        log.info("Sending request to IP Lookup service with ip {}", ip);
        return locationClient.getLocation(ip);
    }
}
