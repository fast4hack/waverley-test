package com.waverley.soft.service;

/**
 * Service that manages cache from LocationService and WeatherService
 */
public interface CacheService {

    /**
     * Delete cache of locations
     */
    void evictLocationCaches();

    /**
     * Delete cache of weather descriptions
     */
    void evictWeatherCaches();
}
