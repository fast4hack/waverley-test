package com.waverley.soft.service;

/**
 * Service that determine weather description by location
 */
public interface WeatherService {
    /**
     * Determine weather description by city name
     * @param city city name
     * @return weather description
     */
    String getWeatherDescriptionByCity(String city);
}
