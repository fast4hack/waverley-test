package com.waverley.soft.service;

import com.waverley.soft.dto.WelcomeMessageDTO;

/**
 * Service for generating welcome message for user that contains weather for its location
 */
public interface WelcomeMessageService {
    /**
     * Generate welcome message
     * @param ip IP address for determining location
     * @return welcome message response
     */
    WelcomeMessageDTO getWelcomeMessageByIP(String ip);
}
