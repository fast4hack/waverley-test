package com.waverley.soft.validation;

import com.waverley.soft.exception.NotValidIPAddressException;

/**
 * Validator of IP addresses
 */
public interface IPAddressValidator {
    /**
     * Validate IP address
     * @param ip IP address
     * @throws NotValidIPAddressException indicates that IP is not valid
     */
    void validate(String ip) throws NotValidIPAddressException;
}
