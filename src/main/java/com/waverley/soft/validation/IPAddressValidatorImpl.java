package com.waverley.soft.validation;

import com.waverley.soft.exception.NotValidIPAddressException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Slf4j
@Component
public class IPAddressValidatorImpl implements IPAddressValidator {
    // Regex for digit from 0 to 255.
    private static final String ZERO_TO_255 = "(\\d{1,2}|(0|1)\\d{2}|2[0-4]\\d|25[0-5])";

    // Regex for a digit from 0 to 255 and
    // followed by a dot, repeat 4 times.
    // this is the regex to validate an IP address.
    private static final String IP_ADDRESS_REGEX
        = ZERO_TO_255 + "\\."
        + ZERO_TO_255 + "\\."
        + ZERO_TO_255 + "\\."
        + ZERO_TO_255;

    private static final Pattern IP_PATTERN = Pattern.compile(IP_ADDRESS_REGEX);

    @Override
    public void validate(String ip) throws NotValidIPAddressException {
        log.info("Validating ip address {}", ip);
        if (ip == null || !IP_PATTERN.matcher(ip).matches()) {
            throw new NotValidIPAddressException(ip);
        }
    }
}
