package com.waverley.soft.exception;

/**
 * Thrown when IP address is not valid
 */
public class NotValidIPAddressException extends RuntimeException {

    public static final String EXCEPTION_MESSAGE_TEMPLATE = "IP address %s is not valid!";

    /**
     * @param ip not valid IP address
     */
    public NotValidIPAddressException(String ip) {
        super(String.format(EXCEPTION_MESSAGE_TEMPLATE, ip));
    }
}
