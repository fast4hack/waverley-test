package com.waverley.soft.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown when Weather can't be determined by location
 */
@Getter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class WeatherNotDeterminedException extends RuntimeException {

    public static final String EXCEPTION_MESSAGE = "Weather was not determined for your location! Response status - %d";
    private final int status;

    /**
     * @param status status code that returns from Weather API
     */
    public WeatherNotDeterminedException(int status) {
        super(String.format(EXCEPTION_MESSAGE, status));
        this.status = status;
    }
}
