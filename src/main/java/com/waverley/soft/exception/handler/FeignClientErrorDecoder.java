package com.waverley.soft.exception.handler;

import com.waverley.soft.exception.LocationNotDeterminedException;
import com.waverley.soft.exception.WeatherNotDeterminedException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Decoder of errors from LocationClient and WeatherClient
 */
@Slf4j
@Component
public class FeignClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        switch (methodKey) {
            case "LocationClient#getLocation(String)":
                log.error("Error while sending request to Location Service API. Method key - {}. Response code - {}", methodKey, response.status());
                return new LocationNotDeterminedException(response.status());
            case "WeatherClient#getWeather(String,String)":
                log.error("Error while sending request to Weather Service API. Method key - {}. Response code - {}", methodKey, response.status());
                return new WeatherNotDeterminedException(response.status());
            default:
                log.error("Error while sending request to client with methodKey - {}. Response status code - {}", methodKey, response.status());
                return new Exception(response.reason());
        }
    }
}
