package com.waverley.soft.exception.handler;

import com.waverley.soft.dto.ErrorResponse;
import com.waverley.soft.exception.LocationNotDeterminedException;
import com.waverley.soft.exception.NotValidIPAddressException;
import com.waverley.soft.exception.WeatherNotDeterminedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseBody
    @ExceptionHandler(LocationNotDeterminedException.class)
    public ResponseEntity<ErrorResponse> handleLocationException(LocationNotDeterminedException e) {
        log.error("Handling LocationNotDeterminedException", e);
        return new ResponseEntity<>(ErrorResponse.builder().description(e.getMessage()).errorCode(e.getStatus()).build(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(WeatherNotDeterminedException.class)
    public ResponseEntity<ErrorResponse> handleWeatherException(WeatherNotDeterminedException e) {
        log.error("Handling WeatherNotDeterminedException", e);
        return new ResponseEntity<>(ErrorResponse.builder().description(e.getMessage()).errorCode(e.getStatus()).build(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(NotValidIPAddressException.class)
    public ResponseEntity<ErrorResponse> handleIPValidationException(NotValidIPAddressException e) {
        log.error("Handling NotValidIPAddressException", e);
        return new ResponseEntity<>(ErrorResponse.builder().description(e.getMessage()).errorCode(HttpStatus.BAD_REQUEST.value()).build(), HttpStatus.BAD_REQUEST);
    }
}
