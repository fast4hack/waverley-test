package com.waverley.soft.exception;

import lombok.Getter;

/**
 * Thrown when location can't be determined by IP
 */
@Getter
public class LocationNotDeterminedException extends RuntimeException {
    public static final String EXCEPTION_MESSAGE_TEMPLATE = "Location was not determined by IP! Status code - %d";
    private final int status;

    /**
     * @param status status code from HTTP request to Location API
     */
    public LocationNotDeterminedException(int status) {
        super(String.format(EXCEPTION_MESSAGE_TEMPLATE, status));
        this.status = status;
    }
}
