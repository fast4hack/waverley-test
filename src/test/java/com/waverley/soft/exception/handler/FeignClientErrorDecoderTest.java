package com.waverley.soft.exception.handler;

import com.waverley.soft.exception.LocationNotDeterminedException;
import com.waverley.soft.exception.WeatherNotDeterminedException;
import feign.Request;
import feign.RequestTemplate;
import feign.Response;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FeignClientErrorDecoderTest {

    private final FeignClientErrorDecoder errorDecoder = new FeignClientErrorDecoder();

    @Test
    public void decodeLocationExceptionTest() {
        Exception ex = errorDecoder.decode("LocationClient#getLocation(String)", getResponse());
        assertTrue(ex instanceof LocationNotDeterminedException);
        assertEquals("Location was not determined by IP! Status code - 400", ex.getMessage());
    }

    @Test
    public void decodeWeatherExceptionTest() {
        Exception ex = errorDecoder.decode("WeatherClient#getWeather(String,String)", getResponse());
        assertTrue(ex instanceof WeatherNotDeterminedException);
        assertEquals("Weather was not determined for your location! Response status - 400", ex.getMessage());
    }

    @Test
    public void decodeDefaultExceptionTest() {
        Exception ex = errorDecoder.decode("SomeClient#getSome()", getResponse());
        assertEquals("Exception reason", ex.getMessage());
    }

    private Response getResponse() {
        return Response.builder()
            .request(
                Request
                    .create(Request.HttpMethod.GET,
                        "",
                        Collections.emptyMap(),
                        Request.Body.create("data")
                        , new RequestTemplate()))
            .reason("Exception reason")
            .status(400)
            .build();
    }
}
