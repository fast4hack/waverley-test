package com.waverley.soft.service;


import com.waverley.soft.dto.LocationResponseDTO;
import com.waverley.soft.service.impl.WelcomeMessageServiceImpl;
import com.waverley.soft.validation.IPAddressValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


public class WelcomeMessageServiceTest {

    private WelcomeMessageServiceImpl messageService;
    private WeatherService weatherService;
    private LocationService locationService;

    @BeforeEach
    public void setup() {
        locationService = mock(LocationService.class);
        weatherService = mock(WeatherService.class);
        messageService = new WelcomeMessageServiceImpl(weatherService, locationService, mock(IPAddressValidator.class));
        ReflectionTestUtils.setField(messageService, "WELCOME_MESSAGE_TEMPLATE", "Welcome, it looks %s in %s, %s today");
    }

    @Test
    public void getWelcomeMessageByIPTest() {
        doReturn(LocationResponseDTO.builder().country("Ukraine").city("Kyiv").build()).when(locationService).getLocationByIP(any(String.class));
        doReturn("Sunny").when(weatherService).getWeatherDescriptionByCity(eq("Kyiv"));

        assertThat(messageService.getWelcomeMessageByIP("127.0.0.1").getMessage()).isEqualTo("Welcome, it looks Sunny in Kyiv, Ukraine today");

    }
}
