package com.waverley.soft.service;

import com.waverley.soft.api.LocationClient;
import com.waverley.soft.dto.LocationResponseDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
@DirtiesContext
public class LocationServiceITTest {

    @Autowired
    private LocationService locationService;

    private final LocationClient locationClient = mock(LocationClient.class);

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(locationService, "locationClient", locationClient);
    }

    @Test
    public void getLocationByIPTest() {
        when(locationClient.getLocation(eq("77.120.21.172"))).thenReturn(LocationResponseDTO.builder().country("Ukraine").city("Kyiv").build());

        LocationResponseDTO location = locationService.getLocationByIP("77.120.21.172");

        assertEquals("Kyiv", location.getCity());
        assertEquals("Ukraine", location.getCountry());
        verify(locationClient, times(1)).getLocation(eq("77.120.21.172"));
    }
}
