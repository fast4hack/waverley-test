package com.waverley.soft.service;

import com.waverley.soft.api.LocationClient;
import com.waverley.soft.api.WeatherClient;
import com.waverley.soft.dto.LocationResponseDTO;
import com.waverley.soft.dto.Weather;
import com.waverley.soft.dto.WeatherResponseDTO;
import com.waverley.soft.dto.WelcomeMessageDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
@DirtiesContext
public class WelcomeMessageServiceITTest {
    @Autowired
    private WelcomeMessageService welcomeMessageService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private WeatherService weatherService;

    private final LocationClient locationClient = mock(LocationClient.class);
    private final WeatherClient weatherClient = mock(WeatherClient.class);

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(weatherService, "weatherClient", weatherClient);
        ReflectionTestUtils.setField(locationService, "locationClient", locationClient);
    }

    @Test
    public void welcomeMessageTest() {
        when(locationClient.getLocation(eq("127.0.0.1"))).thenReturn(LocationResponseDTO.builder().city("Kyiv").country("Ukraine").build());
        when(weatherClient.getWeather(eq("Kyiv"), anyString()))
            .thenReturn(WeatherResponseDTO.builder()
                .weather(Collections.singletonList(new Weather("Sunny")))
                .build());

        WelcomeMessageDTO message = welcomeMessageService.getWelcomeMessageByIP("127.0.0.1");

        assertEquals("Welcome, it looks Sunny in Kyiv, Ukraine today", message.getMessage());
        verify(locationClient, times(1)).getLocation(eq("127.0.0.1"));
        verify(weatherClient, times(1)).getWeather(eq("Kyiv"), anyString());
    }
}
