package com.waverley.soft.service;

import com.waverley.soft.api.WeatherClient;
import com.waverley.soft.dto.Weather;
import com.waverley.soft.dto.WeatherResponseDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
@DirtiesContext
public class WeatherServiceITTest {
    @Autowired
    private WeatherService weatherService;

    private final WeatherClient weatherClient = mock(WeatherClient.class);

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(weatherService, "weatherClient", weatherClient);
    }

    @Test
    public void getWeatherDescriptionTest() {
        when(weatherClient.getWeather(eq("Kyiv"), anyString()))
            .thenReturn(
                WeatherResponseDTO.builder()
                    .weather(Collections.singletonList(new Weather("Sunny")))
                    .build());
        String weather = weatherService.getWeatherDescriptionByCity("Kyiv");
        assertEquals("Sunny", weather);
        verify(weatherClient, times(1)).getWeather(eq("Kyiv"), anyString());
    }
}
