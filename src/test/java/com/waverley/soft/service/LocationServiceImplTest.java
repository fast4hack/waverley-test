package com.waverley.soft.service;

import com.waverley.soft.api.LocationClient;
import com.waverley.soft.dto.LocationResponseDTO;
import com.waverley.soft.service.impl.LocationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocationServiceImplTest {

    private LocationServiceImpl locationService;
    private LocationClient locationClient = mock(LocationClient.class);

    @BeforeEach
    public void setup() {
        locationService = new LocationServiceImpl(locationClient);
    }

    @Test
    public void getLocationByIPTest() {
        when(locationClient.getLocation(contains("127.0.0.1"))).thenReturn(LocationResponseDTO.builder()
            .country("Ukraine")
            .city("Kyiv")
            .build());
        LocationResponseDTO location = locationService.getLocationByIP("127.0.0.1");
        assertThat(location.getCity()).isEqualTo("Kyiv");
        assertThat(location.getCountry()).isEqualTo("Ukraine");
    }
}
