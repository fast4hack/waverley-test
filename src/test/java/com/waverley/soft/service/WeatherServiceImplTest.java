package com.waverley.soft.service;

import com.waverley.soft.api.WeatherClient;
import com.waverley.soft.dto.Weather;
import com.waverley.soft.dto.WeatherResponseDTO;
import com.waverley.soft.service.impl.WeatherServiceIml;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.*;

public class WeatherServiceImplTest {
    private WeatherServiceIml weatherServiceIml;
    private WeatherClient weatherClient = mock(WeatherClient.class);

    @BeforeEach
    public void setup() {
        weatherServiceIml = new WeatherServiceIml(weatherClient);
    }

    @Test
    public void getWeatherDescriptionByCityTest() {
        when(weatherClient.getWeather(contains("Kyiv"), any())).thenReturn(WeatherResponseDTO.builder()
            .weather(
                Collections.singletonList(
                    new Weather("Sunny")
                )
            )
            .build());
        assertThat(weatherServiceIml.getWeatherDescriptionByCity("Kyiv")).isEqualTo("Sunny");
        verify(weatherClient, times(1)).getWeather(ArgumentMatchers.contains("Kyiv"), any());
    }
}
