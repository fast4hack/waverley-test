package com.waverley.soft.validation;

import com.waverley.soft.exception.NotValidIPAddressException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IPAddressValidatorTest {

    private IPAddressValidator validator = new IPAddressValidatorImpl();

    @Test
    public void validationTestForValidIP() {
        validator.validate("000.12.12.034");
        validator.validate("121.234.12.12");
    }

    @Test
    public void validationForNotValidIP() {
        NotValidIPAddressException ex = assertThrows(NotValidIPAddressException.class, () -> validator.validate("000.12.234.23.23"));
        assertEquals("IP address 000.12.234.23.23 is not valid!", ex.getMessage());
    }
}
